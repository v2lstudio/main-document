<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Sondages | Put-in</title>
<link href="css/sondages.css" rel="stylesheet" type="text/css">
<link href="css/index.css" rel="stylesheet" type="text/css">
</head>
<header>
	<div id="nav">
		<ul id="navbar">
			<a href="sondages.php"><li id="active" class="bold">SONDAGES</li></a>
			<a href="projet.php"><li class="bold">LE PROJET</li></a>
			<a href="index.php"><li class="bold">ACCUEIL</li></a>
		</ul>
		<a href="connexion.php"><button id="connexion">connexion</button></a>
	</div>
</header>

<body>
	
	<div id="tablediv" align="center">
		<a href="créesondage.php"><button id="sondages">CRée un sondage</button></a>
		<?php
	
		include 'bdd.php';
	
		$reponse = $bdd->query('SELECT * FROM `sondage`');
		while ($donnees = $reponse->fetch()){
		?>
		<table>	
			<tr>
			<th><?php echo $donnees['Nsondage'];?></th>
			</tr>
			
			<tr>
			<td><a  href="sondage.php?id=<?php echo $donnees['idsondage']; ?>" id="bouton">Participer au sondage</a></td>
			</tr>
		</table>
		<?php } ?>
	</div>
	</div>
	
</body>
</html>