<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Connexion | Put-in</title>
<link href="css/inscription.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Contrail+One" rel="stylesheet">
</head>

<body>
	<div align="center" id="titrebar">
		<span class="incline">connexion</span>
	</div>
<div align="center" id="box">
 		<form method="post" action="">
  			<p1 class="incline">Pseudonyme</p1><br>
	  		<input id="cadre" type="text" name="nom"></input><br>
  		
  			<p1 class="incline">mot de passe</p1><br>
	  		<input id="cadre" type="password" name="password"></input><br>
	  
			<button type="submit" id="inscription">connexion</button>
		</form>
<footer>
	<div align="center">
		<a href="inscription.php" ><button id="inscription">Inscription</button></a>
	</div>
	<div id="footerbar">
	
		<p3>put-in projet 2017</p3>
		<div align="right">
			<img id="studio" src="images/v2l.png">
		</div>
	</div>
</footer>
</body>
</html>

	<?php
			if(isset($_POST['nom']) && isset($_POST['password'])){
	
			$nom = $_POST['nom'];	
			$password = $_POST['password'];	
				
			include 'bdd.php';
			$requete=$bdd->prepare('SELECT count(*) as compteur FROM `utilisateur` WHERE `nom`=:nom AND `password` =:password');
			
			$requete->bindParam(':nom', $nom);
			$requete->bindParam(':password', $password);
			
				$requete->execute();
				
			$donnees = $requete->fetch();
				
			if ($donnees['compteur'] < 1)	
			{ 
				echo 'Compte ou Mot de passe faux ou inexistant !'; 
			} 
			if ($donnees['compteur'] > 0)	
			{ 
				session_start();
		
				$_SESSION['nom'] = $_POST['nom'];
				$_SESSION['password'] = $_POST['password'];
				
				echo 'Connexion en cour ...'; 
				header('Location: index.php');
			} 
			
		}
				
		?>
